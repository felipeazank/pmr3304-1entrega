Este é o site de informações sobre Son Goku, o maior Super-Saiyajin e lutador de todos os tempos

Neste site, você encontrará

- Biografia de Son-Goku
- Principais batalhas que lutou 
- Breve história do personagem 

{{< figure src="https://images.eurogamer.net/2020/articles/2020-05-09-12-11/dragon-ball-todas-as-transformacoes-son-goku-1589022707794.jpg/EG11/resize/1200x-1/dragon-ball-todas-as-transformacoes-son-goku-1589022707794.jpg" alt=Imagem >}}