---
title: "Biografia"
date: 2021-10-24T07:13:22-03:00
draft: false 
---

Son-Goku (ou apenas Goku) é o nome terráqueo dado ao Saiyajin Kakarotto. Pouco após seu nascimento no planeta Vegeta, Goku foi enviado a terra por seu Pai Bardok em decorrência da eminente destruição do planeta dos Saiyajins por obra do temível Freeza. Ao chegar na terra, Goku foi adotado por um velho mestre de luta chamado Mestre Kame, o qual treinou Goku para ser um grande guerreiro. Na vida adulta, Goku se casou com Chi Chi e teve um filho chamado Gohan.

Muito tempo depois, Goku conhece seu irmão mais velho, Raditz e finalmente descobre sobre sua origem Saiyajin. Na batalha contra Raditz, Goku acaba sacrificando sua vida, após formar sua primeira aliança com Piccolo, seu antigo inimigo. No além, Goku continua seu treinamento com o Sr.Kaiô onde aprende as técnicas do Kaioken e da Genki Dama, técnicas usadas quando teve que voltar ao mundo terreno para salvar o dia contra o príncipe dos Sayiajins: Vegeta. 

{{< figure src="https://64.media.tumblr.com/bf10064450956d4d23d62208901463eb/tumblr_pc2zsyTybx1t1656jo1_400.jpg" alt=Raditz width="500" height="600" class="center" >}}
