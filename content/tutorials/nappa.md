---
title: "Goku vs Nappa"
date: 2021-09-18T23:28:40-03:00
draft: false
---

Primeira batalha de Goku após voltar da morte. Nessa batalha ele enfrenta "O Grande Nappa" Sayiajin mais velho e mais poderoso que Raditz. Antes da chegada de Goku, Nappa luta e vence diversos amigos terráqueos de Goku, entre eles: Tenshinhan, Caos, Kuririn e Gohan (filho de Goku). 

Durante essa batalha, Goku mostra muito do que aprendeu com Sr.Kaiô no plano espiritual, mostrando pela primera vez uma de suas principais habilidades, o Kaioken, técnica que deixa seus golpes mais poderosos mas com o custo de seu extremo desgaste físico após o uso. 

Nessa luta, Goku também aproveita para mostrar seu poder de luta o que gera a célebre frase do anime: "É mais de 8000!" dita por Vegeta.

{{< figure src="https://pbs.twimg.com/media/DfA6ckVXkAAJv0c.jpg" alt=Nappa width="500" height="600" class="center" >}}