---
title: "Goku e Piccolo vs Raditz"
date: 2021-09-18T23:28:40-03:00
draft: false
---

Uma das primeiras batalhas de Dragon Ball Z, Goku foi forçado a se aliar com Piccolo (seu antigo inimigo) com o objetivo de derrotar o alien Raditz, um Sayiajin que veio a terra procurando Goku. Durante a disputa, foi descoberto que Raditz era irmão de Goku e que seu nome de nascença é Kakarotto.

Depois de muita luta, para eliminar Raditz e salvar a terra, Goku imobiliza seu oponente para que Piccolo possa dar o golpe final, fazendo com que Goku se sacrificasse no processo. 

{{< figure src="https://i0.wp.com/i.pinimg.com/originals/2c/87/10/2c871079db33f0c32669549c41f54e4d.jpg" alt=Raditz width="500" height="600" class="center" >}}