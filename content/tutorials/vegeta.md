---
title: "Goku vs Vegeta"
date: 2021-09-18T23:27:57-03:00
draft: false
---

Hoje conhecida como uma das maiores lutas dos Animes, a última batalha da "Saga dos Saiyajins" mostra o esperado duelo entre Goku e Vegeta, o principe dos Sayiajins. 

Durante essa batalha, Goku e Vegeta mostram seus picos de poder, lutando no decorrer de diversos episódios até a exaustão de ambos. Em determinado ponto do duelo, Kuririn e Gohan ajudam Goku a vencer. 

É também dessa disputa em que Goku mostra uma das habilidade mais célebres da série, a Genki Dama, técnica que usa a energia de harmonia de todas as criaturas vivas nos arredores. 

No fim da disputa, Goku e seus amigos conquistam a vitória, mas Vegeta escapa, iniciando assim, umas das principais rivalidades do mundo dos Animes, relação essa que, no futuro se tornaria uma grande amizade entre iguais. 

{{< figure src="http://pa1.narvii.com/6295/af57ee784a25bdf083c64c778414f17f4f955a52_00.gif" alt=Nappa width="500" height="600" class="center" >}}
